with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;

with GNAT.Command_Line;
with GNAT.Source_Info; use GNAT.Source_Info;

with Logging ;

package body cli is

   package boolean_text_io is new Enumeration_IO (Boolean);
   use boolean_text_io;

   procedure SwitchHandler
     (Switch : String; Parameter : String; Section : String)
   is
   begin
      Put ("SwitchHandler " & Switch);
      Put (" Parameter " & Parameter);
      Put (" Section " & Section);
      New_Line;
   end SwitchHandler;

   procedure ProcessCommandLine is
      Config : GNAT.Command_Line.Command_Line_Configuration;
   begin

      GNAT.Command_Line.Set_Usage
        (Config,
         Help =>
           NAME & " " & VERSION & " " & Compilation_ISO_Date & " " &
           Compilation_Time,
         Usage => "sockgw");
      GNAT.Command_Line.Define_Switch
        (Config, Verbose'access, Switch => "-v", Long_Switch => "--verbose",
         Help                           => "Output extra verbose information");
      GNAT.Command_Line.Define_Switch
        (Config, Verbosity'access, Switch => "-L=",
         Long_Switch => "--logging-level=",
         Initial => logging.WARNING ,
         Default => logging.WARNING ,
         Help => "Logging Level");
      GNAT.Command_Line.Define_Switch
        (Config, ClientOption'access, Switch => "-c",
         Long_Switch => "--client", Help => "Start sample client");
      GNAT.Command_Line.Define_Switch
        (Config, TargetOption'access, Switch => "-u",
         Long_Switch => "--udptarget", Help => "Start a UDP target server");

      GNAT.Command_Line.Define_Switch
        (Config, GatewayPortNo'access, Switch => "-g?",
         Long_Switch                          => "--gateway?",
         Initial => DEFAULT_GATEWAY_PORT ,
         Default => DEFAULT_GATEWAY_PORT ,
         Help => "Gateway Stream Socket Port Number");
      GNAT.Command_Line.Define_Switch
        (Config, TargetPortNo'access, Switch => "-t?",
         Initial => DEFAULT_TARGET_PORT ,
         Default => DEFAULT_TARGET_PORT ,
         Long_Switch => "--target?", Help => "Target UDP Socket Port Number");

      GNAT.Command_Line.Getopt (Config, SwitchHandler'access);
      if Verbose
      then
         ShowCommandLineArguments ;
      end if ;
      logging.Current_Logging_Level := Verbosity ;
   end ProcessCommandLine;

   function GetNextArgument return String is
   begin
      return GNAT.Command_Line.Get_Argument (Do_Expansion => True);
   end GetNextArgument;

   procedure ShowCommandLineArguments is
   begin
      Put ("Verbose ");
      Put (Verbose);
      New_Line;
      Put ("Verbosity ");
      Put(Verbosity);
      New_Line ;

      Put ("Client Option ");
      Put (ClientOption);
      New_Line;
      Put ("Target Option ");
      Put (TargetOption);
      New_Line;

      Put ("Gateway Port No ");
      Put (GatewayPortNo);
      New_Line;
      Put ("Target Port No ");
      Put (TargetPortNo);
      New_Line;

   end ShowCommandLineArguments;

end cli;
