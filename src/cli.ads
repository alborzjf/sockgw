with Ada.Strings.Unbounded ; use Ada.Strings.Unbounded ;
with gnat.strings ;

package cli is

   VERSION : string := "V01" ;
   NAME : String := "sockgw" ;
   Verbose : aliased boolean ;
   Verbosity : aliased integer ;

   DEFAULT_GATEWAY_PORT : constant := 1025 ;
   DEFAULT_TARGET_PORT : constant := 1027 ;

   GatewayPortNo : aliased Integer ;
   TargetPortNo : aliased Integer  ;
   ClientOption : aliased boolean  ;
   TargetOption : aliased boolean ;
   HelpOption : aliased boolean ;

   procedure ProcessCommandLine ;
   function GetNextArgument return String ;
   procedure ShowCommandLineArguments ;

end cli ;
