pragma Ada_2012;
with Ada.Text_Io; Use Ada.Text_Io ;
with Ada.Integer_Text_IO ; use Ada.Integer_Text_IO ;
package body clientele is

   protected body ServerType is
      procedure Connect(port : integer; node : string := "127.0.0.1" ) is
         ServerAddr : GNAT.Sockets.Sock_Addr_Type ;
      begin
         GNAT.Sockets.Create_Socket( sock ) ;
         ServerAddr.Addr := GNAT.Sockets.Inet_Addr(node);
         ServerAddr.Port := GNAT.Sockets.Port_Type (port);
         GNAT.Sockets.Connect_Socket(sock,ServerAddr) ;
         Put("Connected to "); Put(port); Put_Line(node);
         stream := GNAT.Sockets.Stream(sock) ;
      end Connect ;

      procedure Send(data : Ada.Streams.Stream_Element_Array) is
      begin
         stream.Write(data);
      end Send ;
   end ServerType ;

   procedure StartClient (name : string; cadence : Duration := 1.0) is
   begin
      if name = "String"
      then
         declare
            simpleton : StringClientPtrType := new StringClient ;
         begin
            simpleton.Start (cadence) ;
         end ;
      end if ;
   end StartClient;

   task body StringClient is
      dur : Duration ;
      next : Ada.Calendar.Time  ;
      msgnum : integer := 0 ;
   begin
      accept Start(cadence : Duration) do
         begin
            dur := cadence ;
         end ;
         loop
            declare
               use Ada.Calendar;

               msg : string := "This is message" & integer'image(msgnum) & ASCII.LF ;
               msgbytes : Ada.Streams.Stream_Element_Array( 1.. Ada.Streams.Stream_ELement_Count(msg'length));
               for msgbytes'Address use msg'Address;
            begin
               msgnum := msgnum + 1 ;
               if Verbose
               then
                  Put ("Sent > No: "); Put(msgnum) ; Put(" ");
                  Put(msg'length);
                  Put_Line(" bytes");
               end if ;
               next := Ada.Calendar.Clock + dur ;
               server.Send(msgbytes);
               delay until next ;
            end ;
         end loop ;
      end Start;

   end StringClient ;

end clientele;
