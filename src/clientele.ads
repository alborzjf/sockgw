with Ada.Calendar ;
with Ada.Streams.Stream_IO ;
with GNAT.Sockets ;

package clientele is
   Verbose : boolean := true ;
   protected type ServerType is
      procedure Connect(port : integer; node : string := "127.0.0.1") ;
      procedure Send(data : Ada.Streams.Stream_Element_Array) ;
   private
      sock : GNAT.Sockets.Socket_Type ;
      stream : GNAT.Sockets.Stream_Access ;
   end ServerType ;

   server : ServerType ;

   procedure StartClient( name : string ; cadence : Duration := 1.0 ) ;
private
   task type StringClient is
      entry Start (cadence : Duration ) ;
   end StringClient ;
   type StringClientPtrType is access StringClient ;
end clientele ;
