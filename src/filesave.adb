with Ada.Calendar ;
with Ada.Strings.Unbounded ;
with Ada.Strings.Fixed ; use Ada.Strings.Fixed ;


with GNAT.Calendar ;

package body filesave is
   function ZImage(val : integer; fieldwidth : integer ) return String is
      vstr : String := Integer'Image(val) ;
   begin
      if vstr'length > fieldwidth
      then
         return vstr(2..vstr'last) ;
      end if ;

      for c in vstr'Range
      loop
         if vstr(c) = ' '
         then
            vstr(c) := '0' ;
         end if ;
      end loop ;

      return vstr ;
   end ZImage ;

   protected body DataArchive is
      procedure Open(basename : string ) is
      begin
         nameprefix := ada.strings.unbounded.To_Unbounded_String(basename) ;
         OpenNext ;
      end Open ;
      procedure Close is
      begin
         Ada.Streams.Stream_IO.Close(stream);
      end Close ;

      procedure Save(data : Ada.Streams.Stream_Element_Array) is
      begin
         Ada.Streams.Stream_IO.Write(stream,data) ;
      end Save ;

      procedure OpenNext is
         now : Ada.Calendar.Time := Ada.Calendar.Clock ;
         Year       : Ada.Calendar.Year_Number;
         Month      : Ada.Calendar.Month_Number;
         Day        : Ada.Calendar.Day_Number;
         Hour       : GNAT.Calendar.Hour_Number;
         Minute     : GNAT.Calendar.Minute_Number;
         Second     : GNAT.Calendar.Second_Number;
         Sub_Second : GNAT.Calendar.Second_Duration ;
      begin
         if Ada.Streams.Stream_IO.Is_Open(stream)
         then
            Ada.Streams.Stream_IO.Close(stream);
         end if ;
         GNAT.Calendar.Split_At_Locale(now , Year, Month, Day, Hour, Minute, Second, Sub_Second) ;
         declare
            yyyy : string := ZImage(Year,4) ;
            mm : string := ZImage(Month,2);
            dd : string := ZImage(Day,2) ;
            hh : string := ZImage(Hour,2) ;
            min : string := ZImage(Minute,2) ;
            ss : string := ZImage(Second,2) ;
            filename : string := Ada.Strings.Unbounded.To_String(nameprefix) &
                                 "_" & yyyy & mm & dd &
                                 "_" & hh & min & ss & ".bin" ;
         begin
            Ada.Streams.Stream_IO.Create(stream,ada.streams.Stream_IO.Out_File,filename);
         end ;
      end OpenNext ;
   end DataArchive ;


end filesave ;
