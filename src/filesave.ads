with Ada.Streams ;
with Ada.Streams.Stream_IO ;
with Ada.Strings.Unbounded ;
with Ada.Integer_Text_IO;

package filesave is
   protected type DataArchive is
      procedure Open(basename : string ) ;
      procedure OpenNext ;
      procedure Close ;
      procedure Save(data : Ada.Streams.Stream_Element_Array) ;
   private
      nameprefix : ada.strings.Unbounded.Unbounded_String ;
      stream : Ada.Streams.Stream_IO.File_Type ;
   end DataArchive ;
   archive : DataArchive ;
end filesave ;
