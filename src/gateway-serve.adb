with Ada.Text_IO;         use Ada.Text_IO;
with Ada.Integer_Text_IO; use Ada.Integer_Text_IO;
with Ada.Exceptions;
with Ada.Calendar ;

with Interfaces.C; use Interfaces.C;

with GNAT.Sockets;
with GNAT.Calendar ;
with GNAT.Time_Stamp;
with GNAT.Source_Info;

with filesave ;
separate (gateway)
procedure Serve (port : Integer; target : Integer) is
   Mysocket : GNAT.Sockets.Socket_Type;
   myaddr   : GNAT.Sockets.Sock_Addr_Type;

   mytarget : GNAT.Sockets.Socket_Type ;
   mytargetaddr : GNAT.Sockets.Sock_Addr_Type ;

   mytask      : ServiceProviderPtr;
   clientcount : Integer := 0;

begin
   GNAT.Sockets.Create_Socket (Mysocket);
   myaddr.Addr := GNAT.Sockets.Any_Inet_Addr;
   myaddr.Port := GNAT.Sockets.Port_Type (port);
   GNAT.Sockets.Bind_Socket (Mysocket, myaddr);

   mytargetaddr.Addr := GNAT.Sockets.Inet_Addr( "127.0.0.1" );
   mytargetaddr.Port := GNAT.Sockets.Port_Type (target) ;
   GNAT.Sockets.Create_Socket(mytarget, GNAT.Sockets.Family_Inet , mode=>GNAT.Sockets.Socket_Datagram) ;

   filesave.archive.Open("archive") ;
   loop
      GNAT.Sockets.Listen_Socket (Mysocket);
      loop
      declare
         use GNAT.Sockets;
         clientsocket : GNAT.Sockets.Socket_Type;
         clientaddr   : GNAT.Sockets.Sock_Addr_Type;
         accstatus    : GNAT.Sockets.Selector_Status;
         started : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour(Ada.Calendar.Clock);
      begin
         GNAT.Sockets.Accept_Socket
           (Mysocket, clientsocket, clientaddr, 5.0, Status => accstatus);
            if accstatus = GNAT.Sockets.Completed then
               clientcount := clientcount + 1;

               Put ("Received a connection Client: ");
               Put (GNAT.Sockets.Image (clientaddr));
               New_Line;

               mytask      := new ServiceProvider;
               mytask.Provide (clientcount, clientsocket,mytarget,mytargetaddr);
            else
               if Verbose
               then
                  Put_Line("Server timer");
               end if ;
               declare
                  hnow : GNAT.Calendar.Hour_Number := GNAT.Calendar.Hour( Ada.Calendar.Clock ) ;
               begin
                  if hnow /= started
                  then
                     filesave.archive.OpenNext ;
                     started := hnow ;
                  end if ;
            end ;
         end if;
         end;
      end loop ;
   end loop;
end Serve;
