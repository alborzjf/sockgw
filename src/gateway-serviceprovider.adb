with Text_IO; use Text_IO;
with filesave ;

separate (gateway)
task body ServiceProvider is
   use Ada.Streams ;
   myid     : Integer;
   myclient : GNAT.Sockets.Socket_Type;

   mytarget : GNAT.Sockets.Socket_Type;
   mytargetaddr : GNAT.Sockets.Sock_Addr_Type;

   buffer   : String (1 .. BUFFER_SIZE);
   msglen   : Integer;
   bufbytes : Stream_Element_Array(1 .. Stream_Element_Count(buffer'Length) ) ;
   for bufbytes'Address use buffer'Address ;
   sentcount : Ada.Streams.Stream_Element_Count ;
   stream : GNAT.Sockets.Stream_Access ;
begin
   accept Provide (id : Integer; client : GNAT.Sockets.Socket_Type; target : GNAT.Sockets.Socket_Type; targetaddr : GNAT.Sockets.Sock_Addr_Type) do
      begin
         myid     := id;
         myclient := client;
         mytarget := target ;
         mytargetaddr := targetaddr ;
      end;
      Text_IO.Put_Line ("Server No " & Integer'Image (myid));
   end Provide ;

   declare
       name   : String                     := Integer'image (myid);
   begin
      stream := GNAT.Sockets.Stream (myclient);
         loop
            ReadLine (stream, buffer, msglen);
            GNAT.Sockets.Send_Socket(mytarget,bufbytes(1..Stream_Element_Count(msglen)),sentcount,mytargetaddr);
            if Verbose
            then
               Put (name & "> ");
               Put("Forwarded ");
               Put(sentcount'Img);
               Put_Line(" bytes");
            end if ;

            filesave.archive.Save( bufbytes(1..Stream_Element_Count(msglen)) );
         end loop;
      exception
         when others =>
            GNAT.Sockets.Shutdown_Socket (myclient);
            GNAT.Sockets.Close_Socket(myclient);
      end;
end ServiceProvider;
