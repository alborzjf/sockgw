with Text_Io; use Text_IO ;
with logging ;

separate (gateway)
procedure Target( port : Integer ) is
   mysocket : GNAT.Sockets.Socket_Type ;
   myaddr : GNAT.Sockets.Sock_Addr_Type ;
   from : GNAT.Sockets.Sock_Addr_Type ;
   Data : Ada.Streams.Stream_Element_Array (1 .. 512);
   Last : Ada.Streams.Stream_Element_Offset;
   StrData : String( Integer(Data'First) .. Integer(Data'Last)) ;
   for StrData'Address use Data'Address ;
begin
   GNAT.Sockets.Create_Socket(mysocket,GNAT.Sockets.Family_Inet,GNAT.Sockets.Socket_Datagram) ;
   GNAT.Sockets.Set_Socket_Option (mysocket, GNAT.Sockets.Socket_Level, (GNAT.Sockets.Reuse_Address, True));

   myaddr.Addr := GNAT.Sockets.Inet_Addr("127.0.0.1") ;
   myaddr.Port := GNAT.Sockets.Port_Type(port) ;
   GNAT.Sockets.Bind_Socket(mysocket,myaddr) ;
   loop
      GNAT.Sockets.Receive_Socket (mysocket , Data, Last, From);
      -- Put(Last'Img);
      -- Put(" bytes from ");
      -- Put_Line(GNAT.Sockets.Image(from.Addr));
      logging.Log(StrData(1..Integer(Last)));
   end loop ;
end Target ;
