with Ada.Streams; use Ada.Streams;
with GNAT.Sockets ;

package body gateway is

   BUFFER_SIZE : constant := 256 ;


   procedure ReadLine( Stream : in out GNAT.Sockets.Stream_Access ;
                       buffer : in out string ;
                       last : out Integer ;
                       terminator : Character := ascii.LF ) is
      NewChar : Character ;
   begin
      last := 0 ;
      loop
         Character'Read(Stream , NewChar);
         last := last + 1 ;
         buffer(last) := NewChar ;
         exit when NewChar = terminator
                or last >= buffer'length ;
      end loop ;
   end ReadLine ;


   procedure WriteLine( Stream : in out GNAT.Sockets.Stream_Access ;
                        buffer : string ;
                        terminator : Character := ascii.LF ) is
   begin
      for c in buffer'range
      loop
         Character'Write(stream , buffer(c));
      end loop ;
      Character'Write(stream , terminator) ;
   end WriteLine ;

   task type ServiceProvider is
      entry Provide( id : integer ; client : GNAT.Sockets.Socket_Type ; target : GNAT.Sockets.Socket_Type; targetaddr : GNAT.Sockets.Sock_Addr_Type) ;
   end ServiceProvider ;

   type ServiceProviderPtr is access ServiceProvider ;
   task body ServiceProvider is separate ;
   procedure Serve( port : Integer ;
                    target : Integer ) is separate ;
   procedure Client( port : Integer ) is separate ;
   procedure Target( port : Integer ) is separate ;

end gateway ;
