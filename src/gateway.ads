package Gateway is
   Verbose : boolean ;
   procedure Serve( port : Integer ;
                    target : Integer ) ;
   procedure Client( port : Integer ) ;
   procedure Target( port : Integer ) ;

end Gateway ;
