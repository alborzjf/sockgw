pragma Ada_2012;
with Text_Io ; use Text_Io ;
with GNAT.Time_Stamp ;

package body logging is

   ---------
   -- Log --
   ---------

   procedure Log
     (message : string; level : Logging_Level_Type := INFORMATIONAL)
   is
   begin
      if level <= Current_Logging_Level
      then
         Put(GNAT.Time_Stamp.Current_Time); Put(" [") ;
         case level is
            when FATAL => Put("FATAL");
            when SEVERE => Put("SEVERE");
            when ERROR => Put("ERROR");
            when WARNING => Put("WARNING");
            when INFORMATIONAL => Put("INFORMATIONAL");
            when others => Put("Level: "); Put(Logging_Level_Type'Image(level)) ;
         end case ;
         Put("] ");
         Put_Line(message);
      end if ;
   end Log;

end logging;
