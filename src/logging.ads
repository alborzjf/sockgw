package logging is

   subtype Logging_Level_Type is integer range 0..100 ;

   INFORMATIONAL : constant Logging_Level_Type := 40 ;
   WARNING : constant Logging_Level_Type := 30 ;
   ERROR :   constant Logging_Level_Type := 20 ;
   SEVERE : constant Logging_Level_Type := 10 ;
   FATAL : constant Logging_Level_Type := 0 ;

   Current_Logging_Level : aliased Logging_Level_Type := Logging_Level_Type'Last ;
   procedure Log( message : string ;
                  level : Logging_Level_Type := INFORMATIONAL );

end logging ;
