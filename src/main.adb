with Text_IO; use Text_IO;
with GNAT.Command_Line ;
with Cli ;
with Gateway ;
with Logging ;

procedure Main is

begin
   cli.ProcessCommandLine ;
   Logging.Log("Stream Socket Gateway Started");
   if cli.ClientOption
   then
      Gateway.Client(cli.GatewayPortNo);
   elsif cli.TargetOption
   then
      Gateway.Target(cli.TargetPortNo) ;
   else
      Gateway.Serve(cli.GatewayPortNo, cli.TargetPortNo);
   end if ;

exception
      when GNAT.Command_Line.Exit_From_Command_Line => null ;
end Main;
